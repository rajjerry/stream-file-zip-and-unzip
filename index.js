const file = require('fs');
const zlib = require('zlib');
var gzip = zlib.createGzip();
var rstream = file.createReadStream('image.jpg');
var wstream = file.createWriteStream('pic.gz');

rstream.pipe(gzip).pipe(wstream).on('finish', function() {
    console.log('Chaining Stream with Image compress. Done Compresing');
});
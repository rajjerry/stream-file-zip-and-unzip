const fs = require('fs');
const cols = 5;
createCSVs([20, 50]);

async function createCSVs(sizes) {
    sizes.forEach(async size => {
        const writeStream = fs.createWriteStream(`${size}.csv`);
        for (let i = 0; i < size; i++) {
            await writeRow(writeStream);
        }
        writeStream.end();
    });
}

async function writeRow(writeStream) {
    return new Promise((resolve, reject) => {
        writeStream.write(randomRow(), 'utf8', () => resolve());
    });
}

function randomRow() {
    const cells = [];
    for (let i = 0; i < cols; i++) {
        cells.push(randomString(5));
    }
    console.log('Random Row : ', cells.join(',') + '\n');
    return cells.join(',') + '\n';
}


function randomString(length) {
    let string = '';
    for (let i = 0; i < length; i++) {
        string += randomChar();
    }
    console.log('Random String : ', string);
    return string;
}

function randomChar() {
    return String.fromCharCode(Math.floor(Math.random() * (122 - 65) + 65));;
}